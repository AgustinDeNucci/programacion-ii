package calculator;

public class Calculator extends Menu implements calculator.ICalculator  {
    private Double first_number;
    private Double second_number;
    private Double result;

    public Double getFirst_number() {
        return first_number;
    }

    public void setFirst_number(Double first_number) { this.first_number = first_number; }

    public Double getSecond_number() {
        return second_number;
    }

    public void setSecond_number(Double second_number) {
        this.second_number = second_number;
    }

    public Double getResult() { return result; }

    public void setResult(Double result) {
        this.result = result;
    }

    @Override
    public double Addition(double first_number, double second_number) {
        setResult(Operator.ADDITION.apply(getFirst_number(),getSecond_number()));
        return getResult();
    }

    @Override
    public double Subtraction(double first_number, double second_number) {
        setResult(Operator.SUBTRACTION.apply(getFirst_number(),getSecond_number()));
        return getResult();
    }

    @Override
    public double Multiplication(double first_number, double second_number) {
        setResult(Operator.MULTIPLICATION.apply(getFirst_number(),getSecond_number()));
        return getResult();
    }

    @Override
    public double Division(double first_number, double second_number) {
        setResult(Operator.DIVISION.apply(getFirst_number(),getSecond_number()));
        return getResult();
    }

}
