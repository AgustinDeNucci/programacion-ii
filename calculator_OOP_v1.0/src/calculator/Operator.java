package calculator;

enum Operator {

    ADDITION("+") {
        double apply(double first_number, double second_number) {
            return first_number + second_number;
        }
    },
    SUBTRACTION("-") {
        double apply(double first_number, double second_number) {
            return first_number - second_number;
        }
    },
    MULTIPLICATION("*") {
        double apply(double first_number, double second_number) {
            return first_number * second_number;
        }
    },
    DIVISION("/") {
        double apply(double first_number, double second_number) {
            return first_number / second_number;
        }
    };
    public final String symbol;

    Operator(String symbol) {
        this.symbol = symbol;
    }

    abstract double apply(double first_number, double second_number);

}