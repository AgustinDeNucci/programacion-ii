package calculator;
import java.util.Scanner;

public class Menu{

    public static void run() {
        boolean exit = false;

        Calculator numbers = new Calculator();
        Scanner reader = new Scanner(System.in);
        Input input = new Input();

        numbers.setFirst_number(input.getFirst_number());
        numbers.setSecond_number(input.getSecond_number());

        while (!exit) {
            System.out.println("1. Addition");
            System.out.println("2. Subtraction");
            System.out.println("3. Multiplication");
            System.out.println("4. Division");
            System.out.println("5. Exit");

            System.out.println("Choose operation");
            int option = reader.nextInt();

            switch (option) {
                case 1 -> {
                    numbers.Addition(numbers.getFirst_number(), numbers.getSecond_number());
                    System.out.println(numbers.getResult());
                }
                case 2 -> {
                    numbers.Subtraction(numbers.getFirst_number(), numbers.getSecond_number());
                    System.out.println(numbers.getResult());
                }
                case 3 -> {
                    numbers.Multiplication(numbers.getFirst_number(), numbers.getSecond_number());
                    System.out.println(numbers.getResult());
                }
                case 4 -> {
                    numbers.Division(numbers.getFirst_number(), numbers.getSecond_number());
                    System.out.println(numbers.getResult());
                }
                case 5 -> exit = true;
                default -> System.out.println("Wrong option");
            }
        }
    }
}
