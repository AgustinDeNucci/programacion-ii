package calculator;

public interface ICalculator {

    double Addition(double first_number, double second_number);
    double Subtraction(double first_number, double second_number);
    double Multiplication(double first_number, double second_number);
    double Division(double first_number, double second_number);

}
