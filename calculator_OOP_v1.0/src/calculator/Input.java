package calculator;
import java.util.Scanner;
public class Input extends Calculator {

    public Input() {
        Scanner reader = new Scanner(System.in);

        System.out.println("Enter first number: ");
        setFirst_number(reader.nextDouble());
        System.out.println("Enter second number: ");
        setSecond_number(reader.nextDouble());
    }
}
