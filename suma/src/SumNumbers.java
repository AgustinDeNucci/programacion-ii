import java.util.Scanner;
//Usamos la clase Scanner para pedir números por consola.

public class SumNumbers
{
    public static void main( String[] args )
    {
        int n1, n2, sum;

        Scanner reader = new Scanner( System.in );
        //Mediante la clase System.in instanciamos el Scanner

        System.out.print( "Introduzca primer número: " );
        n1 = reader.nextInt();
        //Para leer cada uno de los numeros nos apoyamos el método .nextInt().

        System.out.print( "Introduzca segundo número: " );
        n2 = reader.nextInt();

        sum = n1 + n2;

        System.out.println( "La suma de " + n1 + " más " + n2 + " es " + sum + "." );
    }
}
